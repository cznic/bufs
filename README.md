bufs
====

Package bufs implements a simple buffer cache.

 installation: go get modernc.org/bufs

documentation: http://godoc.org/modernc.org/bufs
